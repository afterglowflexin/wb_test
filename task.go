/*
	Сделать обход каталога (все файловое дерево - вся структура) большой вложенности с большим объемом файлов (1 млрд)
	не нагружающий дисковое IO с минимальными затратами ресурсов. Результат записать в файл.
*/

package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
)

func main() {
	err := Walk("../..", "./file.txt")
	if err != nil {
		log.Panicln(err)
	}
}

// принимает корневую директорию для обхода и файл для записи результата
func Walk(root string, outputFile string) error {

	f, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer f.Close()

	// функция filepath.Walk обходит все файлы и директории в корневой директории. Для каждого файла и директории вызывается функция,
	// в которой проверяется, директория это или нет (!info.IsDir). Если нет, то путь файла записывается в outputFile.
	err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() {
			_, err := fmt.Fprintln(f, path)
			if err != nil {
				return err
			}
		}

		return nil
	})

	if err != nil {
		return err
	}

	return nil
}
